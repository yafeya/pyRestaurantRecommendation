import unittest
import json
import logging
import os

from RestaurantApiControllers import extract_path

class RouteTest(unittest.TestCase):
    
    def test_extract_path(self):
        json_data='''
            {  
                "routes":[  
                    {  
                        "steps":[  
                            {  
                            "path":"116.33997237424,40.011180849862;116.34007567937,40.009373054922"
                            },                            
                            {  
                           "path":"116.34007567937,40.009373054922;116.34052662874,40.009380653703"
                            }]
                    }
                ]
            }
        '''
        path_data = extract_path(json_data)
        self.assertEqual(len(path_data.split(';')),3)
        self.assertIn('116.33997237424,40.011180849862;',path_data)
        self.assertIn('116.34052662874,40.009380653703',path_data)
        self.assertIn('116.34007567937,40.009373054922;',path_data)

if __name__ == '__main__':
    unittest.main()