import time
from RestaurantApiControllers import CoordinatesConvertApiController


if __name__ == '__main__':
    # location = '39.999734144579,116.33970166089'
    coordinates = '39.916397320930635,116.34269658595059'
    ak = 'jLKWXCmDwGdfddhBvaB0GmqBr8K5gwum'
    sk = 'ZphXAtI0goU2aRcOGFpzPsWmZOY00UNa'

    controller = CoordinatesConvertApiController(ak=ak, sk=sk)
    start_time = time.time()
    converted = controller.get(coordinates=coordinates)
    end_time = time.time()

    print(f'Before: {coordinates}, after: {converted}')
